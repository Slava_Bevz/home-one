//-----------------------------------------------Задание №1------------------------------------

// разработайте функцию-конструктор, которая будет создавать объект Human (человек). Создайте массив.
// объектов и резлизуйте функцию, которая будет сортировать элементы массива по значению свойства
// Age по возрастанию или по убыванию.

// function Human(name, become, age, growth, weight) {
//     this.name = name;
//     this.become = become;
//     this.age = age;
//     this.growth = growth;
//     this.weight = weight;
// }

// let armen = new Human('Armen', 'male', 24, 167, 60);
// let stepan = new Human('Stepan', 'male', 36, 164, 55);
// let zoriana = new Human('Zoriana', 'Female', 66, 188, 81);
// let alex = new Human('Alex', 'male', 25, 175, 69);
// let rudolf = new Human('Rudolf', 'male', 19, 173, 79);

// let people = [armen, stepan, zoriana, alex, rudolf];

// people.sort(function(a, b){
//     return a.age-b.age // по возрастанию
//     // return b.age-a.age   // по убыванию
// });

// for (let i = 0; i < people.length; i++) {
//     document.write(`${people[i].name} | ${people[i].become} | ${people[i].age} | ${people[i].growth} cm | ${people[i].weight} kg <br />`);
// }

//-----------------------------------------------задание №2------------------------------------

// разработайте функцию-конструктор, которая будет создавать объект Human (человек), добавьте на свое
// усмотрение свойства и методы в этот объект. Подумайте, какие методы и свойства следует сделать
// уровня экземпляра, а какие уровня функции-конструктора.

function Human(name, age, become) {
    this.name = name;
    this.age = age; //свойство уровня єкземпляра
    this.become = become;
    this.eat = function () {
        return console.log("sneakers"); // метод єкземпляра
    };
}
const stepan = new Human("Stepan", 13, "Male");
const zoriana = new Human("Zoriana", 23, "Famale");

Human.prototype.comingOfAge = function (age) {
    if (age >= 18) {
        // метод уровня функции-конструктора
        return console.log("ok");
    }
    return console.log("no");
};
Human.body = true; //свойство уровня функции-конструктора
console.log(stepan);
console.log(zoriana);
stepan.eat();
stepan.comingOfAge();
console.log(stepan.age);
console.log(Human.body);

//-----------------------------------------------задание №3-------------------------------------

// Создайте пример демонстрирующий назначение метода toString().

// function Lama(name, age, motto) {
//     this.name = name;
//     this.age = age;
//     this.motto = motto;
// }

// const lama1 = new Lama('Степан', 6, 'Lama-Степан БАКЛАЖАН!!!');

// Lama.prototype.toString = function lamaToString() {
//     return `Имя: ${this.name} возраст: ${this.age} лет. Жизненный девиз: ${this.motto}`;
// };

// console.log(lama1.toString());

//----------------------------------------------------------------------------------------------
s;
